// Завдання
// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.

// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.

// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel.
// Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача.
// Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.

// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome,  + ім'я користувача.

// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.

// Після введення даних додати перевірку їхньої коректності.
// Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново
// (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).

let userName = "";
let userAge = "";
let userNamePlaceholder = ""; // using to store the placeholder when the users clicks "Cancel"
let userAgePlaceholder = ""; // using to store the placeholder when the users clicks "Cancel"
let ageConfirmation = false; // using to check confirmation for users aged 18-22

for (let i = 0; i < 1; ) {
  userName = prompt("Input your name:", userNamePlaceholder);
  if (userName === null) {
    userName = ""; // changing the "userName" value from null to empty string after the user clicked "Cancel"
  } else {
    userNamePlaceholder = userName; // saving the placeholder when the user inputted any data in the "userName" field and clicked "OK"
  }

  userAge = prompt("Input your age (years):", userAgePlaceholder);
  if (userAge === null) {
    userAge = ""; // changing the "userAge" value from null to empty string after the user clicked "Cancel"
  } else {
    userAgePlaceholder = userAge; // saving the placeholder when the user inputted any data in the "userAge" field and clicked "OK"
  }

  if (userName.length > 0 && userAge > 0) i++; // checking that the "userName" isn't an empty string AND "userAge" is a positive number
}

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  ageConfirmation = confirm("Are you sure you want to continue?");
  ageConfirmation === true
    ? alert(`Welcome, ${userName}`)
    : alert("You are not allowed to visit this website");
} else alert(`Welcome, ${userName}`);
